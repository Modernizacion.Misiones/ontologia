Hoy en dı́a los ciudadanos hacen un uso intensivo de la tecnologı́a de comunicación móvil, y exigen a los proveedores de servicios públicos iformación compleja y sofisticada. Esperan respuestas en formato de datos ricos que ofrezcan diferentes alternativas antes de tomar una
decisión.

Para satisfacer estas demandas, las agencias de servicios de los gobiernos deben orquestar una gran cantidad de información de diversas fuentes y formatos, y brindarlos en los terminales de datos que la gente comúnmente utiliza: computadoras, netbooks, tablets y smartphones.

Para lograr esta orquestación de servicios públicos, los gobiernos necesitan una plataforma que proporcione accesibilidad, interoperabilidad y compatibilidad de los datos y los servicios basados en la web.

Para superar estos problemas, se propone un modelo para la representación conceptual de las unidades de organización del Estado, vistos como entidades del Gobierno Electrónico, basado en ontologı́as diseñadas bajo los principios de los Datos Abiertos Vinculados, lo que permite la
extracción automática de información a través de las máquinas, que apoya al proceso de tomas de decisiones gubernamentales y dar a los ciudadanos un acceso integral para encontrar y hacer tramites a través de las tecnologı́as móviles.

La ontologı́a presentada va además acompañada de una especificación de arquitectura de pares o Peer to Peer (P2P) basada en las arquitecturas de tablas de búsqueda distribuidas (DHTs), que denominaremos DHT Semántica. La DHT Semántica está diseñada para permitir la explotación
por parte de Agentes Inteligentes de la ontologı́a especificada, con una alta tolerancia a fallos en nodos de la arquitectura. Estos Agentes asisten en la búsqueda de objetos educativos más allá de la búsqueda por palabras claves. Finalmente, tanto la ontologı́a como la arquitectura se validan
utilizando un conjunto de experiencias educativas on-line reales.

Como una caso práctico, la aplicación del modelo propuesto es una ontologı́a que ahora forma parte del Portal de Datos Abiertos del gobierno de la Provincia de Misiones, ver http://www.datos.misiones.gov.ar/ontologias/.

English version:

Today's citizens make intensive use of mobile communication technology, and demand complex and sophisticated information from public service providers. They expect answers in a data-rich format that offer different alternatives before making a decision.

To meet these demands, government service agencies must orchestrate a wealth of information from diverse sources and formats, and deliver them on the data terminals that people commonly use: computers, netbooks, tablets and smartphones.

To achieve this orchestration of public services, governments need a platform that provides accessibility, interoperability and compatibility of web-based data and services.

To overcome these problems, a model is proposed for the conceptual representation of the organizational units of the State, seen as E-Government entities, based on ontologies designed under the principles of Linked Open Data, which allows the automatic extraction of information through machines, supporting the governmental decision making process and giving citizens a comprehensive access to find and perform procedures through mobile technologies.

The presented ontology is also accompanied by a Peer to Peer (P2P) architecture specification based on distributed lookup table architectures (DHTs), which we will call Semantic DHT. The Semantic DHT is designed to allow the exploitation by Intelligent Agents of the specified ontology, with a high tolerance to failures in nodes of the architecture. These Agents assist in the search for educational objects beyond keyword search. Finally, both the ontology and the architecture are validated using a set of real online using a set of real on-line educational experiences.

As a case study, the application of the proposed model is an ontology that is now part of the Open Data Portal of the government of the Province of Misiones, 

see http://www.datos.misiones.gov.ar/ontologias/.